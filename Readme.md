## Project instruction
    This is a configuration project based on multi-repositories management for Repo;

    Repo is a tool built on top of Git. Repo helps manage many Git repositories,
    does the uploads to revision control systems, and automates parts of the development workflow. 
    Repo is not meant to replace Git, only to make it easier to work with Git. 
    The repo command is an executable Python script that you can put anywhere in your path.
### How to use ?

* Install repo:

    ```shell script
    $ mkdir -p ~/.bin
    $ PATH="${HOME}/.bin:${PATH}"
    $ curl https://storage.googleapis.com/git-repo-downloads/repo > ~/.bin/repo
    $ chmod a+rx ~/.bin/repo
    ```


*  Init project repositories:

    ```shell script
    $ cd [work directory]
    $ repo init -u git@bitbucket.org:fsdi/ktcandroid-manifest.git
    $ repo sync 
    ```


* More usage of Repo
 ```html
    <a href="https://source.android.com/setup/develop/repo">Repo documents</a>
```

### Existing problem
* Bitbucket code review is not supported, you need to submit PR manually